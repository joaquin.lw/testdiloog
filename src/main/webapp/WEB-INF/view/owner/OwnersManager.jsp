<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
          <%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Owners Manager</title>
</head>
<body>
<h1>Owners Manager</h1>
<%-- <br> 
<form action="/owners/filter?filterType=${filterType}&filter=${filter }" method="get">
<input type="number" name="filter"/>
<select name="filterType">
<option value="all" label="See All" selected="true"/>
<option value="id" label="ID"/>
<option value="dni" label="D.N.I."/>
</select> <input type="submit" value="Filter">
</form> --%>
<br>
<table border="1" items="${listOwner }">
<thead>
<tr>
<td>Id</td>
<td>Full Name</td>
<td>D.N.I.</td>
<td>Direction</td>
<td>Phone</td>
<td>Action</td>
</tr>
</thead>
<core:forEach items="${listOwner}" var="o">
<tr>
<td>${o.id}</td>
<td>${o.fullName }</td>
<td>${o.dni }</td>
<td>${o.direction }</td>
<td>${o.phone }</td>
<td><a href="owners/updateOwners?id=${o.id }">Update</a></td>
</tr>
</core:forEach>
</table>
<br>
<input type="button" value="Add an Owner" onClick="window.location.href='owners/addOwners'; return false";>
</body>
</html>