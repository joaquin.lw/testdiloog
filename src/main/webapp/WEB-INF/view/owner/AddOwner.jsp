<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
      <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Owner Register</title>
</head>
<body>
<h1>Owner's Registration</h1>
<br><br>
<form:form action="confirmRegistration" modelAttribute="owner">
Full Name: <form:input path="fullName"/>
<br>
D.N.I.: <form:input path="dni"/>
<br>
Direction: <form:input path="direction"/>
<br>
Phone: <form:input path="phone"/>
<br><br>
<input type="submit" value="Send">
</form:form>
</body>
</html>