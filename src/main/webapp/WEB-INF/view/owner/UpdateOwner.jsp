<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update Owner ${owner.fullName }</title>
</head>
<body>
<h2>Update the owner ${owner.id }</h1>
<h2>Name: ${owner.fullName }</h2>
<h3>D.N.I.: ${owner.dni }</h3>
<h4>Direction: ${owner.direction }</h4>
<h4>Phone: ${owner.phone }</h4>
<br>
You can edit the name and the direction of the owner
<br>
<form:form action="confirmOwnersUpdate" modelAttribute="owner">
<form:input path="id" type="hidden"/>
Full Name: <form:input path="fullName"/>
<br>
Direction: <form:input path="direction"/>
<br>
<form:input path="phone" type="hidden"/> <form:input path="dni" type="hidden"/>
<br>
<input type="submit" value="Update">
</form:form>

</body>
</html>