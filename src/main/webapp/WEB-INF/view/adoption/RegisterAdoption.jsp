<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Adoption Form</title>
</head>
<body>
<h2>Form for Pet Adoption</h2>
<form:form action="successRegister" modelAttribute="adoption">
Pet ID: <form:input path="pet.id"/> <%-- <form:button action="validatePet">Validate</form:button> --%>

<br><br>
Owner ID: <form:input path="owner.id"/> <%-- <form:button action="validateOwner">Validate</form:button> --%>
<br>
<input type="submit" value="Submit">
</form:form>
</body>
</html>