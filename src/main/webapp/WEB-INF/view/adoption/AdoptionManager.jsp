<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Adoption's Manager</title>
</head>
<body>
<h2>Adoption's Manager</h2>
<br><br>
<form>

<table border="1" items="${listAdoptions }">
<thead>
<tr>
<td>Adoption Id</td>
<td>Adoption Date</td>
<td>Owner's ID</td>
<td>Owner's Full Name</td>
<td>Owner's Direction</td>
<td>Owner's Phone</td>
<td>Pet's ID</td>
<td>Pet's Name</td>
<td>Pets's Age</td>
<td>Pet's Type</td>
</tr>
</thead>
<core:forEach items="${listAdoption}" var="ad">
<tr>
<td>${ad.id}</td>
<td><fmt:formatDate value="${ad.adoptionDate}" type="date"/></td>
<td>${ad.owner.id }</td>
<td>${ad.owner.fullName }</td>
<td>${ad.owner.direction }</td>
<td>${ad.owner.phone }</td>
<td>${ad.pet.id }</td>
<td>${ad.pet.name }</td>
<td>${ad.pet.age }</td>
<td>${ad.pet.type.description }</td>
</tr>
</core:forEach>
</table>

</form>
<input type="button" value="Register a New Adoption" onClick="window.location.href='adoptions/registerAdoptions'; return false";>
</body>
</html>