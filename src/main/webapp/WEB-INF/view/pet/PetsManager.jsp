<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %> 
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Pets Manager</title>
</head>
<body>
<h1>Pets Manager</h1>
<br> <%-- 
<form action="filter" modelAttribute="controller">
<form:input path="controller.filter"/>
<form:select path="controller.filterType" multiple="false">
<form:option value="all" label="See All"/>
<form:option value="id" label="ID"/>
<form:option value="name" label="Name"/>
<form:option value="inAdoption" label="In Adoption"/>
<form:option value="adopted" label="Adopted"/>
</form:select>
Type of Animal: <form:select path="pet.type" multiple="false">
<form:option value="perro" label="Perro"/>
<form:option value="gato" label="Gato"/>
<form:option value="conejo" label="Conejo"/>
<form:option value="pajaro" label="Pajaro"/>
<form:option value="tortuga" label="Tortuga"/>
</form:select>
State:
<form:radiobutton path="typeAnimal" value="Perro"/>Perro
<form:radiobutton path="typeAnimal" value="Gato"/>Gato
<form:radiobutton path="typeAnimal" value="Pajaro"/>Pajaro
<form:radiobutton path="typeAnimal" value="Conejo"/>Conejo
<form:radiobutton path="typeAnimal" value="Tortuga"/>Tortuga
 <input type="submit" value="Filter">
</form> --%>
<br>
<table border="1" items="${listPet }">
<thead>
<tr>
<td>Id</td>
<td>Name</td>
<td>Aprox. Age</td>
<td>State</td>
<td>Check-In</td>
<td>Type</td>
<td>Action</td>
</tr>
</thead>
<core:forEach items="${listPet}" var="pet">
<tr>
<td>${pet.id}</td>
<td>${pet.name }</td>
<td>${pet.age }</td>
<td>${states[pet.state] }</td>
<td><fmt:formatDate value="${pet.checkIn}" type="date"/></td>
<td>${pet.type.description }</td>
<td><a href="pets/updatePets?id=${pet.id }">Update</a></td>
</tr>
</core:forEach>
</table>
<br>
<input type="button" value="Add a New Pet" onClick="window.location.href='pets/addPets'; return false";>
</body>
</html>