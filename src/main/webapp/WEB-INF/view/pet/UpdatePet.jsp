<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %> 
    <%@ taglib prefix = "core" uri = "http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Pet Update ${pet.id }</title>
</head>
<body>
<h2>Update the pet ${pet.id }</h1>
<h2>Name: ${pet.name }</h2>
<h3>Aprox. Age: ${pet.age }</h3>
<h4>State: ${states[pet.state] }</h4>
<h4>Check In Date: <fmt:formatDate value="${pet.checkIn }" type="date"/> </h4>

<br>
You can edit only the name and the age of the pet
<br>
<form:form action="confirmPetsUpdate" modelAttribute="pet">
<form:input path="id" type="hidden"/>
Name: <form:input path="name"/>
<br>
Age: <form:input path="age"/>
<br>
<form:input path="checkIn" type="date" hidden="true"/>
<form:input path="state" type="hidden"/>
<form:input path="type.id" type="hidden"/>
<br>
<input type="submit" value="Update">
</form:form>

</body>
</html>