<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
    <%@ taglib prefix="core" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>New Pet Register</title>
</head>
<body>
<h1>Pet's Registration</h1>
<br><br>
<form:form action="confirmRegistration" modelAttribute="pet">
Name: <form:input path="name"/>
<br>
Aprox. Age: <form:input path="age"/>
<br>
CheckIn Date: <form:input type="date" path="checkIn"/>
<br>
Type of Animal: <form:select path="type.id">
<core:forEach items="${listTypes }" var="types">
<form:option value="${types.id }" label="${types.description }"/>
</core:forEach>
</form:select>
<br>
State: <form:radiobutton path="state" value="0" label="In Adoption" checked="true"/>
<form:radiobutton path="state" value="1" label="Adopted"/>
<form:radiobutton path="state" value="2" label="Gone"/>
<br><br>
<input type="submit" value="Send">
</form:form>
</body>
</html>