package repository;

import java.util.*;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import model.*;

@Repository
public class AdoptionRepository {

	@Autowired
	private static SessionFactory sesFactory = new Configuration().configure("hibernate.cfg.xml")
			.addAnnotatedClass(Adoption.class).addAnnotatedClass(Owner.class).addAnnotatedClass(Pet.class)
			.addAnnotatedClass(TypeAnimal.class).buildSessionFactory();
	
	@Transactional
	public static List<Adoption> getAllAdoptions() {
		List<Adoption> list;
		Session session = sesFactory.openSession();
		try {
			list = session.createQuery("from Adoption").getResultList();
		} finally {
			session.close();
		}
		return list;
	}
	
	@Transactional
	public static boolean addAdoption(Adoption ad) {
		Session session = sesFactory.openSession();
		boolean flag = false;
		try {
			session.beginTransaction();
			session.save(ad);
			session.getTransaction().commit();
			flag = true;
			System.out.println("Successful register");
		} finally {
			session.close();
		}
		return flag;
	}
	
}
