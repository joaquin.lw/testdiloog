package repository;

import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.*;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import model.Pet;
import model.TypeAnimal;

@Repository
public class PetRepository {

	@Autowired
	public static SessionFactory sesFactory = new Configuration().configure("hibernate.cfg.xml")
			.addAnnotatedClass(Pet.class).addAnnotatedClass(TypeAnimal.class).buildSessionFactory();
	
	@Transactional
	public static boolean addPet(Pet pet) {
		Session session = sesFactory.openSession();
		boolean flag = false;
		try {
			session.beginTransaction();
			session.save(pet);
			session.getTransaction().commit();
			flag = true;
		} finally {
			session.close();
		}
		return flag;
	}
	
	@Transactional
	public static boolean updatePet(Pet pet) {
		boolean flag = false;
		Session session = sesFactory.openSession();
		try {
			if(PetRepository.getPetById(pet.getId()) != null) {
				session.beginTransaction();
				session.update(pet);
				session.getTransaction().commit();
				flag = true;
			} else {
				System.out.println("No pet found on that ID");
			}
		} finally {
			session.close();
		}
		return flag;
	}
	
	@Transactional
	public static List<Pet> getAllPets(){
		List<Pet> list = null;
		Session session = sesFactory.openSession();
		try {
			list = session.createQuery("from Pet").getResultList();
		} finally {
			session.close();
		}
		return list;
	}
	
	@Transactional
	public static List<TypeAnimal> getAllTypes(){
		List<TypeAnimal> types = null;
		Session session = sesFactory.openSession();
		try {
			types = session.createQuery("from TypeAnimal").getResultList();
		} finally {
			session.close();
		}
		return types;
	}
	
	@Transactional
	public static Pet getPetById(int id) {
		Pet pet;	
		Session session = sesFactory.openSession();
		try {
			session.beginTransaction();
			pet = session.get(Pet.class, id);
			session.getTransaction().commit();
		} finally {
			session.close();
		}
		
		return pet;
	}
	
	@Transactional
	public static List<Pet> getPetByName(String name) {
		List<Pet> list = null;
		Session session = sesFactory.openSession();
		try {
			Query<Pet> query = session.createQuery("from Pet where name like :name", Pet.class);
			query.setParameter("name", name);
			list = query.getResultList();
		} finally {
			session.close();
		}
		return list;
	}
	
	@Transactional
	public static List<Pet> getPetsByType(TypeAnimal type){
		Session session = sesFactory.openSession();
		List<Pet> list = null;
		try {
			Query<Pet> query = session.createQuery("from Pet where typeId=:typeId");
			query.setParameter("typeId", type.getId());
			list = query.getResultList();
		} finally {
			session.close();
		}
		return list;
	}
	
	@Transactional
	public static List<Pet> getPetsByState(String state){
		List<Pet> list = null;
		Session session = sesFactory.openSession();
		try {
			Query<Pet> query = session.createQuery("from Pet where state=:state");
			query.setParameter("state", state);
			list = query.getResultList();
		}finally {
			session.close();
		}
		return list;
	}

	@Transactional
	public static TypeAnimal getTypeById(int id) {
		TypeAnimal type = null;
		Session session = sesFactory.openSession();
		try {
			type = session.get(TypeAnimal.class, id);
		} finally {
			session.close();
		}
		return type;
	}
	
}
