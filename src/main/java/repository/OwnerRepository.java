package repository;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

import model.Owner;

@Repository
public class OwnerRepository {

	@Autowired
	private static SessionFactory sessionFactory = new Configuration().configure("hibernate.cfg.xml")
			.addAnnotatedClass(Owner.class).buildSessionFactory();

	@Transactional
	public static boolean addOwner(Owner o) {
		Session session = sessionFactory.openSession();
		boolean flag = false;
		try {
			session.beginTransaction();
			session.save(o);
			session.getTransaction().commit();
			flag = true;
		} finally {
			session.close();
		}
		return flag;
	}

	@Transactional
	public static List<Owner> getAllOwners() {
		List<Owner> list = null;
		Session session = sessionFactory.openSession();
		try {
			session.beginTransaction();
			list = session.createQuery("from Owner").getResultList();
			session.getTransaction().commit();
			if (list == null) list = new ArrayList<>();
		} finally {
			session.close();
		}
		return list;
	}

	@Transactional
	public static Owner getOwnerById(int id) {
		Owner o = null;
		Session session = sessionFactory.openSession();
		try {
			session.beginTransaction();
			o = session.get(Owner.class, id);
			session.getTransaction().commit();
		} finally {
			session.close();
			// sesFactory.close();
		}
		return o;
	}

	@Transactional
	public static Owner getOwnerByDni(int dni) {
		Owner o = null;
		Session session = sessionFactory.openSession();
		try {
			Query<Owner> query = session.createQuery("from Owner where dni=:ownerDni", Owner.class);
			query.setParameter("ownerDni", dni);
			o = query.getSingleResult();
		} finally {
			session.close();
			// sesFactory.close();
		}
		return o;
	}

	@Transactional
	public static boolean updateOwner(Owner o) {
		boolean flag = false;
		
		Session session = sessionFactory.openSession();
		try {
			if(OwnerRepository.getOwnerById(o.getId()) != null) {
				session.beginTransaction();
				session.update(o);
				session.getTransaction().commit();
				flag = true;
			} else {
				System.out.println("No owner found on that ID");
			}
		} finally {
			session.close();
		}
		return flag;
	}

}
