package model;

import java.util.Date;

import javax.persistence.*;

import org.springframework.beans.factory.annotation.*;

@Entity @Table(name="adoption")
public class Adoption {

	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Autowired
	@Column(name="adoptionDate")
	private Date adoptionDate;
	
	@OneToOne(cascade= {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, fetch=FetchType.EAGER)
	@JoinColumn(name="petId")
	private Pet pet;
	
	@OneToOne(cascade= {CascadeType.DETACH, CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, fetch=FetchType.EAGER)
	@JoinColumn(name="ownerId")
	private Owner owner;


	public Adoption() {
	}


	@Autowired
	public Adoption(Date adoptionDate, Pet pet, Owner owner) {
		this.adoptionDate = adoptionDate;
		this.pet = pet;
		this.owner = owner;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public Date getAdoptionDate() {
		return adoptionDate;
	}


	public void setAdoptionDate(Date adoptionDate) {
		this.adoptionDate = adoptionDate;
	}


	public Pet getPet() {
		return pet;
	}


	public void setPet(Pet pet) {
		this.pet = pet;
	}


	public Owner getOwner() {
		return owner;
	}


	public void setOwner(Owner owner) {
		this.owner = owner;
	}
	
}
