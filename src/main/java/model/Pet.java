package model;

import java.util.Calendar;
import java.sql.Date;
import java.util.GregorianCalendar;

import javax.persistence.*;

import org.springframework.beans.factory.annotation.Autowired;

import repository.PetRepository;

@Entity @Table(name="pet")
public class Pet {

	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="name")
	private String name;
	
	@Column(name="age")
	private String age;
	
	@Column(name="state")
	private int state;
	
	@Column(name="checkIn")
	private Date checkIn;
	
	@Autowired
	@OneToOne(fetch=FetchType.EAGER)
	@JoinColumn(name="typeId")
	private TypeAnimal type;
	
	public static final String[] states = {"In Adoption", "Adopted", "Gone"};

	public Pet() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAge() {
		return age;
	}

	public void setAge(String age) {
		this.age = age;
	}

	public int getState() {
		return state;
	}

	public void setState(int state) {
		this.state = state;
	}

	public Date getCheckIn() {
		return checkIn;
	}

	public void setCheckIn(Date checkIn) {
		this.checkIn = checkIn;
	}

	public TypeAnimal getType() {
		return type;
	}

	public void setType(TypeAnimal type) {
		this.type = type;
	}

	@Override
	public String toString() {
		return "Pet [id=" + id + ", name=" + name + ", age=" + age + ", state=" + state + ", checkIn=" + checkIn
				+ ", type=" + type + "]";
	}

	
	/*
	 * public String[] getStates() { return states; }
	 * 
	 * public void setStates(String[] states) { this.states = states; }
	 */
	
}
