package model;

import javax.persistence.*;

@Entity @Table(name="typeAnimal")
public class TypeAnimal {
	
	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="description")
	private String description;

	public TypeAnimal(String description) {
		this.description = description;
	}

	public TypeAnimal() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
	
}
