package model;

import javax.persistence.*;

import com.sun.istack.NotNull;

@Entity @Table(name="owner")
public class Owner {

	@Id @Column(name="id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	@Column(name="fullName")
	private String fullName;
	
	@Column(name="dni")
	private int dni;
	
	@Column(name="direction")
	private String direction;
	
	@NotNull
	@Column(name="phone")
	private String phone;

	
	public Owner() {
	}

	public Owner(String fullName, int dni, String direction, String phone) {
		this.fullName = fullName;
		this.dni = dni;
		this.direction = direction;
		this.phone = phone;
	}

	public int getId() {
		return id;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public String getPhone() {
		return phone;
	}
	

	public int getDni() {
		return dni;
	}
	

	public void setId(int id) {
		this.id = id;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return "Owner [id=" + id + ", fullName=" + fullName + ", dni=" + dni + ", direction=" + direction + ", phone="
				+ phone + "]";
	}
	
	
	
}
