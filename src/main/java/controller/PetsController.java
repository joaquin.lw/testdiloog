package controller;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.sql.Date;
import java.text.SimpleDateFormat;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import model.Owner;
import model.Pet;
import repository.OwnerRepository;
import repository.PetRepository;

@Controller
@RequestMapping("/pets")
public class PetsController {

	@RequestMapping
	public String petsManager(Model model) {
		List<Pet> petsList = PetRepository.getAllPets();
		if (petsList == null || petsList.isEmpty()) petsList = new ArrayList<>();
		model.addAttribute("listPet", petsList);
		model.addAttribute("states", Pet.states);
//		model.addAttribute("controller", this);
		return "pet/PetsManager";
	}
	
	@RequestMapping("/addPets")
	public String addPet(Model model) {
		Pet pet = new Pet();
		model.addAttribute("pet", pet);
		model.addAttribute("listTypes", PetRepository.getAllTypes());
		return "pet/AddPet";
	}
	
	@RequestMapping("/confirmRegistration")
	public String successRegister(@ModelAttribute("pet") Pet pet) {
		pet.setType(PetRepository.getTypeById(pet.getType().getId()));
		PetRepository.addPet(pet);
		System.out.println("Success Register");
		return "owner/ConfirmRegistration";
	}
	
	@RequestMapping("/updatePets")
	public String updatePets(Model model, @RequestParam("id") int id) {
		Pet pet = PetRepository.getPetById(id);
		model.addAttribute("pet", pet);
		model.addAttribute("listTypes", PetRepository.getAllTypes());
		return "pet/UpdatePet";
	}
	
	@RequestMapping("/confirmPetsUpdate")
	public String confirmUpdate(@ModelAttribute("pet") Pet pet) {
		PetRepository.updatePet(pet);
		System.out.println("Successful Update");
		return "owner/ConfirmUpdate";
	}
}
