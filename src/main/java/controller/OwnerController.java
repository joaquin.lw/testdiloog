package controller;

import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import model.Owner;
import repository.OwnerRepository;

@Controller
@RequestMapping("/owners")
public class OwnerController {

	/*
	 * private String filter; private String filterType;
	 * 
	 * public String getFilter() { return filter; }
	 * 
	 * public void setFilter(String filter) { this.filter = filter; }
	 * 
	 * public String getFilterType() { return filterType; }
	 * 
	 * public void setFilterType(String filterType) { this.filterType = filterType;
	 * }
	 */
	List<Owner> listOwners;

	@RequestMapping()
	public String ownersManager(Model model) {
		listOwners = OwnerRepository.getAllOwners();
		model.addAttribute("listOwner", listOwners);
//		model.addAttribute("controller", this);
		return "owner/OwnersManager";
	}

	@RequestMapping("/addOwners")
	public String addOwner(Model model) {
		Owner owner = new Owner();
		model.addAttribute("owner", owner);
		return "owner/AddOwner";
	}

	@RequestMapping("/confirmRegistration")
	public String confirmRegistration(Model model, @ModelAttribute("owner") Owner o) {
		OwnerRepository.addOwner(o);
		System.out.println("Succesful Register");
		ownersManager(model);
		return "owner/OwnersManager";
	}

	@RequestMapping("/updateOwners")
	public String updateOwner(Model model, @RequestParam int id) {
		Owner o = OwnerRepository.getOwnerById(id);
		model.addAttribute("owner", o);
		return "owner/UpdateOwner";
	}

	@RequestMapping("/confirmOwnersUpdate")
	public String confirmUpdate(Model model, @ModelAttribute("owner") Owner o) {
		System.out.println(o.toString());
		OwnerRepository.updateOwner(o);
		return ownersManager(model);
	}

	
	@RequestMapping(value = "/filter", params = { "filterType", "filter" })
	public String filter(Model model, @RequestParam(defaultValue = "all") String filterType,
			@RequestParam String filter) {
		try {
			if (filterType == null || filterType == "all")
				listOwners = OwnerRepository.getAllOwners();
			else if (filterType == "id")
				listOwners.add(OwnerRepository.getOwnerById(Integer.parseInt(filter)));
			else if (filterType == "dni")
				listOwners.add(OwnerRepository.getOwnerByDni(Integer.parseInt(filter)));
		} catch (NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "Solo se permiten caracteres en el campo filtro", "Error",
					JOptionPane.ERROR_MESSAGE);
		}
		return ownersManager(model);
	}
	  
	 }
