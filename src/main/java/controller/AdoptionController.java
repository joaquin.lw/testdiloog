package controller;

import java.util.Date;
import java.text.SimpleDateFormat;
import java.util.GregorianCalendar;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import model.Adoption;
import model.Owner;
import model.Pet;
import repository.AdoptionRepository;
import repository.OwnerRepository;
import repository.PetRepository;

@Controller
@RequestMapping("/adoptions")
public class AdoptionController {
	
	List<Adoption> listAdoptions;
	@RequestMapping
	public String adoptionsManager(Model model) {
		listAdoptions = AdoptionRepository.getAllAdoptions();
		model.addAttribute("listAdoption", listAdoptions);
		return "/adoption/AdoptionManager";
	}
	
	@RequestMapping("/registerAdoptions")
	public String adoptionForm(Model model) {
		Adoption ad = new Adoption();
		ad.setPet(new Pet());
		ad.setOwner(new Owner());
		model.addAttribute("adoption", ad);
		return "/adoption/RegisterAdoption";
	}
	
	@RequestMapping("/successRegister")
	public String successAdoption(Model model, @ModelAttribute("adoption")Adoption ad) {
		ad.setPet(PetRepository.getPetById(ad.getPet().getId()));
		ad.setOwner(OwnerRepository.getOwnerById(ad.getOwner().getId()));
		if(ad.getPet().getState() == 0 && ad.getOwner() != null) {
			ad.getPet().setState(1);
			ad.setPet(ad.getPet());
			PetRepository.updatePet(ad.getPet());
			ad.setAdoptionDate(new Date());
			AdoptionRepository.addAdoption(ad);
			System.out.println("Success Register");
			return adoptionsManager(model);
		} else {
			System.out.println("Pet already adopted");
			return "/adoption/RegisterAdoption";
		}
	}

}
